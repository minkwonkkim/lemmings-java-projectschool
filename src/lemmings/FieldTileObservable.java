package lemmings;

import java.awt.Graphics;
import java.util.ArrayList;

public abstract class FieldTileObservable implements IFieldTile {
	
	protected FieldTile fieldTile;
	private ArrayList<FieldTileObserver> fieldTObs = new ArrayList<FieldTileObserver>();
	
	public FieldTileObservable() {
		
	}
	
	public FieldTileObservable(FieldTile fieldTile) {
		this.fieldTile = fieldTile;
	}
	
	public void setUpFieldTileBehaviour(FieldTile fieldTile){
		this.fieldTile = fieldTile;
	}

	@Override
	public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
		fieldTile.effect(lemmings, x, y);
	}

	@Override
	public void destroy(Field field, int height, int x, int yStart) {
		fieldTile.destroy(field, height, x, yStart);
	}
	
	public void register(FieldTileObserver o) {
		 fieldTObs.add(o);
	}

	public void unregister(FieldTileObserver o) {
		 fieldTObs.remove(o);
	}

	public void notifyDrawObservers(Graphics g, int posX, int posY) {
		for (FieldTileObserver fieldTOb : fieldTObs) {
			fieldTOb.draw(g, posX, posY);
		}
	}

	@Override
	public boolean isTransparent() {
		return fieldTile.isTransparent();
	}
}
