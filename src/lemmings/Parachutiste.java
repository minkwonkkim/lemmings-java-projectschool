package lemmings;

public class Parachutiste extends NormalLemming {
	
	public Parachutiste(Lemming lem) {
		super(lem);
		setfallingDelay(1);
	}
	
	public Parachutiste() {
		setfallingDelay(1);
	}

	@Override
	protected boolean fallingBehaviour(Field f) {
		if(getMaxHeightFalling() != f.getDim())
			setMaxHeightFalling(f.getDim());

		return super.fallingBehaviour(f);
	}
	
	@Override
	public String toString() {
		return "Parachutiste";
	}
	
	@Override
	public LemmingState copy(Lemming lem) {
		return new Parachutiste(lem);
	}

}
