package lemmings;

import java.util.ArrayList;

public class TeleportTile extends IndestructibleWallTile{

	Coordinate[][] tpsPlacements;
	
	public static TeleportTile inst;
	public TeleportTile(int dimBoard) {
		tpsPlacements = new Coordinate[dimBoard][dimBoard];
		
		setUpFieldTileBehaviour(new FieldTile() {
			
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
				Coordinate coordToCheck = new Coordinate(x, y);
				for(LemmingObservable l : lemmings){
					if(l.getPos().equals(coordToCheck)) {
						Coordinate tpTo = tpsPlacements[x][y];
						l.setPos(tpTo);
					}
				}
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
				// TODO Auto-generated method stub
			}

			@Override
			public boolean isTransparent() {
				return true;
			}
		});
	}

	public TeleportTile(FieldTile fieldTile) {
		super(fieldTile);
	}
	
	public void add(Coordinate firstTp, Coordinate secondTp){
		tpsPlacements[firstTp.getX()][firstTp.getY()] = secondTp;
		tpsPlacements[secondTp.getX()][secondTp.getY()] = firstTp;
	}
		//this.color = Color.BLUE;

}
