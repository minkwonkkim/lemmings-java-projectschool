package lemmings;

import java.awt.Graphics;

public interface FieldTileObserver {

	void draw(Graphics g, int posX, int posY);
}
