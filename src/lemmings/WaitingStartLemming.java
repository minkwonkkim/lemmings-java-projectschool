package lemmings;

public class WaitingStartLemming extends LemmingState{

	public WaitingStartLemming(Lemming lem) {
		super(lem);
	}
	
	@Override
	protected boolean fallingBehaviour(Field f) {
		return false;
	}

	@Override
	protected boolean marchingBehaviour(Field f) {
		return false;
	}

	@Override
	public LemmingState copy(Lemming lem) {
		return new WaitingStartLemming(lem);
	}
	
	@Override
	public String toString() {
		return "Waiting...";
	}
}
