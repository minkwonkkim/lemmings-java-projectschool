package lemmings;

public class Bombeur extends NormalLemming {
	private int explodingCount;
	
	public Bombeur(Lemming lem) {
		super(lem);
		explodingCount = 0;
	}
	
	public Bombeur() {
		explodingCount = 0;
	}
	
	@Override
	protected void move(Field f) {
		explodingCount++;
		if(explodingCount <= 3) {
			super.move(f);
		}else {
			lem.kill();
			f.destroyCircle(lem.getPos().getX(), lem.getPos().getY(), 2);
		}
	}
	
	@Override
	public String toString() {
		return "Bombeur";
	}

	public LemmingState copy(Lemming lem) {
		return new Bombeur(lem);
	}
	
}
