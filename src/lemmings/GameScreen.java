package lemmings;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

public class GameScreen extends JComponent{

	/**
	 * 
	 */
	public final static int WIDTH = 500;
	public final static int HEIGHT_FIELD = 500;
	public final static int HEIGHT_INTERFACE = 105;
	private static final long serialVersionUID = 1L;
	private int dimBoard;
	private FieldView fv;
	private InterfaceView intfv;
	
	//issue with mouseClicked : use mousePressed instead ; mouseClicked triggers events if mouse is pressed AND released
	//if user press and quickly moves mouse as he releases, it will not be registered as a "click"
	private MouseAdapter mouseAdapter = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			super.mouseClicked(e);
			int tabx = (e.getX()*intfv.getDim())/WIDTH;
			int taby = ((e.getY()-HEIGHT_FIELD)*7)/HEIGHT_INTERFACE;
			intfv.makeMove(tabx, taby);
			
			if(e.getY()<= HEIGHT_FIELD) {
				int tabx2 = (e.getX()*dimBoard)/WIDTH;
				int taby2 = (e.getY()*dimBoard)/HEIGHT_FIELD;
				fv.makeMove(tabx2, taby2, intfv.getCurState());
			}
			repaint();
		}
	};
	
	public GameScreen(FieldView fv, InterfaceView intfv, int dimBoard) {
		super();
		this.dimBoard = dimBoard; 
		this.fv = fv;
		this.intfv = intfv;
		addMouseListener(mouseAdapter);
		setOpaque(true);
		setSize(WIDTH, HEIGHT_FIELD+HEIGHT_INTERFACE);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setColor(Color.white);
		g.fillRect(0, 0, HEIGHT_FIELD+HEIGHT_INTERFACE, WIDTH);
		
		fv.drawFields(g);
		fv.drawLemmings(g);
		
		intfv.drawSquare(g);
		intfv.drawMoves(g);
		
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, WIDTH-17, HEIGHT_FIELD);
		g.drawRect(1, 1, WIDTH-19, HEIGHT_FIELD-2);
	}
}
