package lemmings;

public class Charpentier extends NormalLemming {
	
	private int step;
	
	public Charpentier(Lemming lem) {
		super(lem);
		step = 0;
	}

	public Charpentier() {
		step = 0;
	}

	@Override
	protected boolean marchingBehaviour(Field f) {
		Coordinate position = lem.getPos();
		Coordinate checkPopTile = new Coordinate(position.getX() + lem.getDirectionX(), position.getY());
		
		if(step < 5 && !f.containTile(checkPopTile)) {
			f.addWall(1, checkPopTile.getX(), checkPopTile.getY(), SimpleDWT.inst);
			step++;
		}
		return super.marchingBehaviour(f);
	}
	
	@Override
	public String toString() {
		return "Charpentier";
	}
	
	public LemmingState copy(Lemming lem) {
		return new Charpentier(lem);
	}
}
