package lemmings;
import java.awt.Graphics;

public class FieldTileDisplay {
	private FieldTileDisplay() {
		
	};
	
	public static void drawBaseTile(Graphics g, int posX, int posY, int width, int height, int dimBoard) {
		int x = width * posX / dimBoard;
		int xWidth = width / dimBoard;
		int y = height * posY / dimBoard;
		int yWidth = height / dimBoard;
		g.fillRect(x, y, xWidth, yWidth);
		g.drawRect(x, y, xWidth, yWidth);
	}
	
}
