package lemmings;

import java.util.ArrayList;

public class EndTile extends FieldTileObservable {
	public static EndTile inst;
	public EndTile() {
		setUpFieldTileBehaviour(new FieldTile() {
			
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
				for(LemmingObservable l : lemmings){
					if(l.getPos().equals(new Coordinate(x,y)))
						l.arrived();
				}
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
			}

			@Override
			public boolean isTransparent() {
				return true;
			}
		});
	}

	public EndTile(FieldTile fieldTile) {
		super(fieldTile);
	}
}
