package lemmings;

import java.awt.Color;

public class Lemming implements ILemming {
	//LemmingType;
	private Coordinate position;
	private int directionX = -1;
	private int directionY = 0;
	private int totalCountFalling = 0;
	
	private boolean death = false;
	private boolean end = false;
	
	private LemmingState lemmingType;
	
	public Lemming(int posX, int posY) {
		lemmingType = new WaitingStartLemming(this);
		position = new Coordinate(posX, posY);
	}
	
	public Lemming(Lemming l) {
		this.position = new Coordinate(l.position);
		this.directionX = l.directionX;
		this.death = l.death;
		this.end = l.end;
		this.lemmingType = l.getLemmingType().copy(this);
	}
	
	@Override
	public void move(Field f) {
		getLemmingType().move(f);
	}
	
	public void kill() {
		death = true;
	}
	
	public boolean dead() {
		return death;
	}
	
	public void arrived() {
		end = true;
	}
	
	public boolean hasArrived() {
		return end;
	}
	
	public Lemming copy() {
		return new Lemming(position.getX(), position.getY());
	}
	
	public Color color() {
		return new Color((float)0.8,(float)0.8,(float)0.8);
	}

	public int getDirectionX() {
		return directionX;
	}

	public void setDirectionX(int directionX) {
		this.directionX = directionX;
	}
	
	@Override
	public Coordinate getPos() {
		return new Coordinate(position);
	}

	public void changeLemmingState(LemmingState typeLem) {
		this.lemmingType = typeLem.copy(this);
	}

	@Override
	public void setPos(Coordinate goTo) {
		position.setCoord(goTo.getX(), goTo.getY());
		
	}

	public void changeDirection() {
		directionX = -directionX;
	}

	public LemmingState getLemmingType() {
		return lemmingType;
	}

	public int getDirectionY() {
		return directionY;
	}

	public void setDirectionY(int directionY) {
		this.directionY = directionY;
	}

	@Override
	public boolean blocks() {
		return lemmingType.blocks();
	}
	
	public int getTotalCountFalling() {
		return totalCountFalling;
	}

	public void setTotalCountFalling(int totalCountFalling) {
		this.totalCountFalling = totalCountFalling;
	}
}
