package lemmings;

public class Foreur extends NormalLemming {

	private int canFo;
	
	public Foreur(Lemming lem) {
		super(lem);
		canFo = 0;
	}

	public Foreur() {
		canFo = 0;
	}
			
	@Override
	protected boolean fallingBehaviour(Field f) {
		Coordinate position = lem.getPos();
		Coordinate checkDepopTile = new Coordinate(position.getX(), position.getY()+1);
		//chose for the foreur to keep on digging if it tries to dig an undestructible tile, 
		//easy fix if you want it to stop digging after 5 steps (move the f.containTile(checkDepop... condition
		if(canFo < 5 && lem.getTotalCountFalling() < 4) {
			f.destroyWall(1, checkDepopTile.getX(), checkDepopTile.getY()+1);
			f.tileEffectAt(checkDepopTile.getX(), checkDepopTile.getY()+1);
			canFo++;
		}
		return super.fallingBehaviour(f);
	}

	@Override
	protected boolean marchingBehaviour(Field f) {
		return false;
	}

	@Override
	public String toString() {
		return "Foreur";
	}
	
	public LemmingState copy(Lemming lem) {
		return new Foreur(lem);
	}
	
}
