package lemmings;

public interface LemmingObserver {
	public void update();
}
