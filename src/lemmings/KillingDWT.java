package lemmings;

import java.util.ArrayList;

public class KillingDWT extends DestructibleWallTile {

		//this.color = Color.orange;

	public static KillingDWT inst;
	public KillingDWT() {
		setUpFieldTileBehaviour(new FieldTile() {
			
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
			}
			
			@Override
			public void destroy(Field field, int height, int x, int y) {
				ArrayList<Coordinate> circlePoints = new ArrayList<Coordinate>();
				circlePoints.add(new Coordinate(x, y));
				circlePoints.add(new Coordinate(x-1, y));
				circlePoints.add(new Coordinate(x+1, y));
				circlePoints.add(new Coordinate(x, y-1));
				circlePoints.add(new Coordinate(x+1, y+1));
				circlePoints.add(new Coordinate(x-1, y-1));
				
				field.killLemmingsAt(circlePoints);
				destroyParent(field, height, x, y);
			}

			@Override
			public boolean isTransparent() {
				return false;
			}
		});
	}

	public KillingDWT(FieldTile fieldTile) {
		super(fieldTile);
	}
	
}
