package lemmings;


import java.awt.Color;


import java.awt.Graphics;


public class InterfaceView{
	
	public final static int WIDTH = 500;
	public final static int HEIGHT = 105;
	private int posY = 0;

	private final Interface ui;
	private final int numStates;
	
	public InterfaceView(Interface ui) {
		this.ui = ui;
		numStates = ui.getDim();
	}

    
    void drawSquare(Graphics g) {
		for(int i = 0; i < numStates; i++) {
			g.setColor(Color.WHITE);
			g.fillRect(i*(WIDTH/numStates), posY+500, WIDTH / numStates,HEIGHT);
			
			g.setColor(Color.BLACK);
			
			g.drawRect(i*(WIDTH/numStates), posY+500, WIDTH / numStates,HEIGHT);
		}
	}

	void drawMoves(Graphics g) {
		int cellWidth = WIDTH / numStates; 
		int cellHeight = HEIGHT; 
		
		g.setColor(Color.WHITE);
		g.fillRect(ui.getLastSquare()*(WIDTH/numStates), posY+500, cellWidth, cellHeight);
		
		g.setColor(Color.DARK_GRAY);
		g.fillRect(ui.getCurSquare()*(WIDTH/numStates), posY+500, cellWidth, cellHeight);
		
		g.setColor(Color.BLACK);
		g.drawRect(ui.getLastSquare()*(WIDTH/numStates), posY+500, cellWidth, cellHeight);
		g.drawRect(ui.getCurSquare()*(WIDTH/numStates), posY+500, cellWidth, cellHeight);
				
	}
	
	public void makeMove(int tabx, int taby) {
		if(ui.makeMove(tabx, taby))
			System.out.println("Vous avez sélectionné : "+getCurState());
	}
	
	public LemmingState getCurState() {
		return ui.getCurState();
	}
	
	public int getDim() {
		return numStates;
	}
	
}
