package lemmings;

public class Interface {
	
	private LemmingState[] board;	
	private int curSquare;
	private int lastSquare;
	public Interface () {
		curSquare = 7;
		board = new LemmingState[8];
		for (int posX = 0; posX < board.length; posX++) {
			switch (posX) {
				case 0 : board[posX] = new Bloqueur(); break;
				case 1 : board[posX] = new Tunnelier(); break;
				case 2 : board[posX] = new Foreur(); break;
				case 3 : board[posX] = new Bombeur(); break;
				case 4 : board[posX] = new Charpentier(); break;
				case 5 : board[posX] = new Grimpeur(); break;
				case 6 : board[posX] = new Parachutiste(); break;
				case 7 : board[posX] = new NormalLemming(); break;
			}
		}
	}
	
	public int getCurSquare() {
		return curSquare;
	}
	
	public LemmingState getCurState() {
		return board[curSquare];
	}
	
	public int getLastSquare() {
		return lastSquare;
	}
	
	public int getDim() {
		return board.length; 
	}
	
	private void setCell(int posX) {
		lastSquare = curSquare;
		curSquare = posX;
	}
	
	public LemmingState getCell(int posX) {
		return board[posX];
	}

	public boolean makeMove(int posX, int posY) {
		if (posX < 0 || posX >= getDim() || posY < 0 || posY >= getDim())
			return false;
		setCell(posX);
		return true;
	}
	


}
