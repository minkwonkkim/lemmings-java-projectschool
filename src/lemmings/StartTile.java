package lemmings;

import java.util.ArrayList;

public class StartTile extends FieldTileObservable {

	ArrayList<StartToBuild> starts;
	private int delayStart;
	private int countDelay;
	
	public static StartTile inst;
	public StartTile(ArrayList<StartToBuild> starts, int delayStart) {
		this.delayStart = delayStart;
		this.starts = starts;
		
		setUpFieldTileBehaviour(new FieldTile() {
			
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
				// TODO Auto-generated method stub
			}

			@Override
			public boolean isTransparent() {
				return true;
			}
		});
	}

	public StartTile(FieldTile fieldTile) {
		super(fieldTile);
	}
	
	public void popLemmings() {
		if(countDelay == 0) {
		ArrayList<StartToBuild> starts_copy = new ArrayList<>(starts);
		for(StartToBuild start : starts_copy) {
			if(start.getNbNotStarted() == 0) {
				starts.remove(start);
			}else {
				start.popLemming();
			}
		}
		}
		countDelay = (countDelay+1)%delayStart;
	}
	
	public void closeStart(StartToBuild[] start, int i) {
		
	}

}
