package lemmings;

import java.util.ArrayList;

public class StartToBuild {
	ArrayList<LemmingObservable> lemmingsNotStarted;
	private int nbNotStarted;
	
	public StartToBuild() {
		nbNotStarted = 0;
		this.lemmingsNotStarted = new ArrayList<LemmingObservable>();
	}
	
	public void popLemming() {
		LemmingObservable newLemming = lemmingsNotStarted.get(0);
		nbNotStarted = getNbNotStarted() - 1;
		newLemming.changeLemmingState(new NormalLemming());
		lemmingsNotStarted.remove(0);
	}
	
	public void add(LemmingObservable lemming) {
		lemmingsNotStarted.add(lemming);
		nbNotStarted = getNbNotStarted() + 1;
	}

	public int getNbNotStarted() {
		return nbNotStarted;
	}
}
