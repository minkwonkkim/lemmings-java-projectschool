package lemmings;

public class Tunnelier extends NormalLemming {

	public Tunnelier(Lemming lem) {
		super(lem);
	}
	
	public Tunnelier() {

	}
	protected boolean marchingBehaviour(Field field) {
		Coordinate position = lem.getPos();
		Coordinate nextTile = new Coordinate(position.getX() + lem.getDirectionX(), position.getY());
		
		field.destroyWall(1, nextTile.getX(), nextTile.getY()+1);
		
		return super.marchingBehaviour(field);
	}

	@Override
	public String toString() {
		return "Tunnelier";
	}
	
	@Override
	public LemmingState copy(Lemming lem) {
		return new Tunnelier(lem);
	}

	
}
