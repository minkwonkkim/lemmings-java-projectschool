package lemmings;

import java.util.ArrayList;

public class LavaTile extends FieldTileObservable {

	public static LavaTile inst;
	public LavaTile() {
		setUpFieldTileBehaviour(new FieldTile() {
			
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
				for(LemmingObservable l : lemmings){
					if(l.getPos().equals(new Coordinate(x,y)))
						l.kill();
				}
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
			}

			@Override
			public boolean isTransparent() {
				return true;
			}
		});
	}

	public LavaTile(FieldTile fieldTile) {
		super(fieldTile);
	}
}
