package lemmings;

public class ConstructToBuild {

	private Coordinate obsPlacement;
	private FieldTileObservable obsType;
	private char obsShape;
	private int obsLen;
	
	public ConstructToBuild(FieldTileObservable obsType, int obsLen, char obsShape, Coordinate obsPlacement) {
		this.setObsPlacement(obsPlacement);
		this.setObsType(obsType);
		this.setObsLen(obsLen);
		this.setObsShape(obsShape);
	}

	public char getObsShape() {
		return obsShape;
	}

	public void setObsShape(char obsShape) {
		this.obsShape = obsShape;
	}

	public int getObsLen() {
		return obsLen;
	}

	public void setObsLen(int obsLen) {
		this.obsLen = obsLen;
	}

	public Coordinate getObsPlacement() {
		return obsPlacement;
	}

	public void setObsPlacement(Coordinate obsPlacement) {
		this.obsPlacement = obsPlacement;
	}

	public FieldTileObservable getObsType() {
		return obsType;
	}

	public void setObsType(FieldTileObservable obsType) {
		this.obsType = obsType;
	}
}
