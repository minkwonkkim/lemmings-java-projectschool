package lemmings;

public interface ILemming {
	
	Coordinate getPos();

	boolean dead();
	void kill();
	
	boolean hasArrived();
	void arrived();
	
	void move(Field f);

	void setPos(Coordinate goTo);

	boolean blocks();
}
