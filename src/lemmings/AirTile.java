package lemmings;

import java.util.ArrayList;

public class AirTile extends FieldTileObservable {
	public static AirTile inst;
	public AirTile() {
		setUpFieldTileBehaviour(new FieldTile() {
			
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
				// TODO Auto-generated method stub
			}

			@Override
			public boolean isTransparent() {
				return true;
			}
		});
	}

	public AirTile(FieldTile fieldTile) {
		super(fieldTile);
	}
}
