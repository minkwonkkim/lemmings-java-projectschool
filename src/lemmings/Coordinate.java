package lemmings;

public class Coordinate {
		

	private int y;
	private int x;

	public Coordinate(int x, int y) {
		this.y = y;
		this.x = x;
	}

	public Coordinate(Coordinate position) {
		this.x = position.getX();
		this.y = position.getY();
	}

	public int getY() {
		return y;
	}
	
	public void incY(int i) {
		this.y += i;
	}

	public int getX() {
		return x;
	}
	
	public void setCoord(int coordX, int coordY) {
		this.x = coordX;
		this.y = coordY;
	}

	public boolean checkPos(int x, int y) {
		return this.x == x && this.y == y;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		Coordinate other = (Coordinate) o;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	public Coordinate add(int dirX, int dirY) {
		return new Coordinate(x+dirX, y+dirY);
	}

}
