package lemmings;

import java.util.ArrayList;

public class IndestructibleWallTile extends WallTile {
	public static IndestructibleWallTile inst;
	public IndestructibleWallTile() {
		setUpFieldTileBehaviour(new FieldTile() {
			
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
				// TODO Auto-generated method stub
			}

			@Override
			public boolean isTransparent() {
				return false;
			}
		});
	}
	
	public IndestructibleWallTile(FieldTile fieldtile) {
		super(fieldtile);
	}
}
