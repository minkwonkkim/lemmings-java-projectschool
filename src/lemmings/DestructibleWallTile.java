package lemmings;

import java.util.ArrayList;

public abstract class DestructibleWallTile extends WallTile{
	public DestructibleWallTile() {
		setUpFieldTileBehaviour(new FieldTile() {
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
				destroyParent(field, height, x, yStart);
			}

			@Override
			public boolean isTransparent() {
				return false;
			}
		});
	}

	public DestructibleWallTile(FieldTile fieldTile) {
		super(fieldTile);
	}
	
	public void destroyParent(Field field, int height, int x, int yStart) {
		field.addWall(height, x , yStart, AirTile.inst);
	}

}
