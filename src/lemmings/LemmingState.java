package lemmings;

public abstract class LemmingState {
	protected Lemming lem;
	
	private int maxHeightFalling = 5;
	
	private int fallingDelay = 0;
	private int countFalling = 0;
	
	private int jumpingHeight = 2;
	
	public LemmingState(Lemming lem) {
		this.lem = lem;
	}

	public LemmingState() {
		
	}
	
	public void setLemming(Lemming lem) {
		this.lem = lem;
	}
	
	protected void move(Field f) {
		if(countFalling == 0) {
			if(!fallingBehaviour(f)){
				marchingBehaviour(f);
				countFalling = 0;
			}else{
				countFalling = fallingDelay;
			}
		}else{
			countFalling--;
		}
	}

	public int getfallingDelay() {
		return fallingDelay;
	}

	public void setfallingDelay(int fallingDelay) {
		this.fallingDelay = fallingDelay;
	}

	public int getMaxHeightFalling() {
		return maxHeightFalling;
	}

	public void setMaxHeightFalling(int maxHeightFalling) {
		this.maxHeightFalling = maxHeightFalling;
	}

	public int getJumpingHeight() {
		return jumpingHeight;
	}

	public void setJumpingHeight(int jumpingHeight) {
		this.jumpingHeight = jumpingHeight;
	}

	public boolean blocks() {
		return false;
	};
	
	protected abstract boolean fallingBehaviour(Field f);

	protected abstract boolean marchingBehaviour(Field f);
	
	public abstract LemmingState copy(Lemming lem);

	public void move() {
		
	}
}
