package lemmings;

import java.util.ArrayList;
import java.util.List;

public class LemmingObservable implements ILemming {

	private Lemming lemming;
	private List<LemmingObserver> stateObservers;
	
	public LemmingObservable(int startingLineX, int startingLineY) {
		stateObservers = new ArrayList<LemmingObserver>();
		lemming = new Lemming(startingLineX, startingLineY);
	}
	
	public LemmingObservable(LemmingObservable toCopy) {
		this.lemming = new Lemming(toCopy.lemming);
		this.stateObservers = new ArrayList<LemmingObserver>();
		for(LemmingObserver toCopyObs : toCopy.stateObservers) {
			register(toCopyObs);
		}
	}

	@Override
	public void move(Field f) {
		lemming.move(f);
	}

	@Override
	public Coordinate getPos() {
		return lemming.getPos();
	}

	public void setPos(Coordinate goTo) {
		lemming.setPos(goTo);
	}
	
	public void changeLemmingState(LemmingState typeLem) {
		lemming.changeLemmingState(typeLem);
		notifyObservers();
	}

	public void register(LemmingObserver o) {
		stateObservers.add(o);
	}

	public void unregister(LemmingObserver o) {
		stateObservers.remove(o);
	}

	private void notifyObservers() {
		for (LemmingObserver lemObserver : stateObservers) {
			lemObserver.update();
		}
	}

	@Override
	public boolean dead() {
		return lemming.dead();
	}

	@Override
	public boolean hasArrived() {
		return lemming.hasArrived();
	}
	
	public int getDirectionX() {
		return lemming.getDirectionX();
	}

	@Override
	public void kill() {
		lemming.kill();
	}

	@Override
	public void arrived() {
		lemming.arrived();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return lemming.getLemmingType().toString();
	}

	public int getDirectionY() {
		return lemming.getDirectionY();
	}

	@Override
	public boolean blocks() {
		return lemming.blocks();
	}
}
