package lemmings;

import java.util.ArrayList;

public interface IFieldTile {
	public abstract void effect(ArrayList<LemmingObservable> lemmings, int x, int y);
	public abstract void destroy(Field field, int height, int x, int yStart);
	public abstract boolean isTransparent();
}