package lemmings;
import java.util.ArrayList;


public class Field {

	private FieldTileObservable[][] board;
	private static final int DIM_BOARD = 30;
	
	private ArrayList<LemmingObservable> lemmings;
	private static final int NB_LEMMING = 50;
	
	private int deaths = 0;
	private int lemmingsEnd = 0;
	
	ArrayList<Coordinate> startingCoord = new ArrayList<Coordinate>();
	ArrayList<Coordinate> endingCoord = new ArrayList<Coordinate>();

	ArrayList<StartToBuild> starts;
	private int DELAY_START = 3;
	private int nbStarts;
	
	public Field() throws Exception {
		
		setUpStartingPoints();
		this.nbStarts = startingCoord.size();
		setStarts();
		
		setUpEndingPoints();
		
		setTilesInstances();
		setBoard();
		setLemmings();
		
		
		setLevelDesign();
		
	}
	
	public void setLevelDesign() {

		addLine(30,0,28,SimpleDWT.inst);
		
		addLine(30,0,4, SimpleDWT.inst);
		addWall(2,16, 3,SimpleDWT.inst);
		addWall(1,8, 3,KillingDWT.inst);
		Coordinate constructWall = new Coordinate(24,3);
		Coordinate toBuild = new Coordinate(10,20);
		ConstructDWT.inst.addNewConstructWall(this,'_', 5, constructWall, toBuild, IndestructibleWallTile.inst);
		
		addLine(20,0,12, SimpleDWT.inst);
		addWall(6,4, 11,SimpleDWT.inst);
		addWall(6,16, 11,SimpleDWT.inst);
		
		addLine(1,10,15, IndestructibleWallTile.inst);
		addLine(25,0,16, SimpleDWT.inst);
		addBlock(11,12,16,16, SimpleDWT.inst);
		
		addWall(8,18,24,SimpleDWT.inst);
		addBlock(16,20,20,26,SimpleDWT.inst);
		
		addLine(26,0,19, SimpleDWT.inst);
		addLine(5, 26, 20, LavaTile.inst);
		Coordinate tp1 = new Coordinate(10,18);
		Coordinate tp2 = new Coordinate(20,18);
		addTeleport(tp1, tp2);

	}
	
	public void setUpStartingPoints() {
		startingCoord.add(new Coordinate(0,3));
		startingCoord.add(new Coordinate(0,5));
		startingCoord.add(new Coordinate(5,9));
		startingCoord.add(new Coordinate(0,15));
		startingCoord.add(new Coordinate(0,27));
		startingCoord.add(new Coordinate(28,27));
	}
	
	public void setUpEndingPoints() {
		endingCoord.add(new Coordinate(8,27));
		endingCoord.add(new Coordinate(20,27));
		
	}
	
	public void setLemmings() throws Exception {
		if(nbStarts == 0)
			throw(new Exception());
		lemmings = new ArrayList<LemmingObservable>();
		
		int count = 0;
		for(int i = 0; i < NB_LEMMING; i++) {
			LemmingObservable newLem = new LemmingObservable(startingCoord.get(count).getX(), startingCoord.get(count).getY());
			lemmings.add(newLem);
			starts.get(count).add(newLem);
			count = (count+1)%nbStarts;
		}
	}
	
	public void setStarts() {
		starts = new ArrayList<StartToBuild>();
		for(int i = 0; i < nbStarts;i++) {
			starts.add(new StartToBuild());
		}
	}
	
	public void setBoard() {		
		board = new FieldTileObservable[DIM_BOARD][DIM_BOARD];
		
		for(int i = 0; i < DIM_BOARD; i++) {
			for(int j = 0; j < DIM_BOARD; j++) {
				board[i][j] = AirTile.inst;
			}
		}
		
		for(Coordinate coordS : startingCoord) {
			board[coordS.getX()][coordS.getY()] = StartTile.inst;
		}
		
		for(Coordinate coordE : endingCoord) {
			board[coordE.getX()][coordE.getY()] = EndTile.inst;
		}
	}
	
	public void setTilesInstances() {
		SimpleDWT.inst = new SimpleDWT();
		WallTile.inst = new WallTile();
		ConstructDWT.inst = new ConstructDWT(DIM_BOARD);
		EndTile.inst = new EndTile();
		IndestructibleWallTile.inst = new IndestructibleWallTile();
		KillingDWT.inst = new KillingDWT();
		TeleportTile.inst = new TeleportTile(DIM_BOARD);
		StartTile.inst = new StartTile(starts,DELAY_START);
		AirTile.inst = new AirTile();
		LavaTile.inst = new LavaTile();
	}
	
	public void tileEffectAt(int x, int y) {
		try {
			board[x][y].effect(lemmings, x, y);
		}catch(ArrayIndexOutOfBoundsException e) {
			
		}
	}
	
	public boolean isOut(int y) {
		return y < 0; 
	}
	
	public boolean containTile(Coordinate pos) {
		if((pos.getX()+1 >= DIM_BOARD || pos.getX() < 0) || pos.getY() < 0)
			return true;
		else if(pos.getY()+1 >= DIM_BOARD)
			return false;
		return (!board[pos.getX()][pos.getY()].isTransparent());
	}
	
	public boolean containBlocker(Coordinate pos) {
		for(LemmingObservable lemming : lemmings) {
			if(lemming.getPos().equals(pos) && lemming.blocks()) {
				return true;
			}
		}
		return false;
	}
	
	public void moveLemmings() {
			ArrayList<LemmingObservable> l_copy = new ArrayList<LemmingObservable>(lemmings);
			for (LemmingObservable lemming : l_copy) {
				lemming.move(this);
				if(lemming.dead()){
					lemmings.remove(lemming);
					deaths++;
				}else if(lemming.hasArrived()) {
					lemmings.remove(lemming);
					lemmingsEnd++;
				}
			}
	}
	

	public void killLemmingsAt(ArrayList<Coordinate> posLems) {
		for (LemmingObservable lemming : lemmings) {
			if(posLems.contains(lemming.getPos()))
				lemming.kill();
	}
	}

	public void setUpLemmings(FieldView fv) {
		fv.setLemmings(lemmings);
	}
	
	public FieldTileObservable getTypeFieldAt(int x, int y) {
		if((x+1 >= DIM_BOARD || x < 0) || y < 0)
			return WallTile.inst;
		return board[x][y];
	}
	
	public int getDim() {
		return DIM_BOARD;
	}
	
	public int curLemmingsEnd() {
		return lemmingsEnd;
	}
	
	public int curLemmingDead() {
		return deaths;
	}
	
	public int nbLemmings() {
		return NB_LEMMING;
	}

	public void makeMove(Coordinate tabCoord, LemmingState typeLem) {
		for(LemmingObservable lemming : lemmings) {
			if(lemming.getPos().equals(tabCoord)) {
				lemming.changeLemmingState(typeLem);
			}
		}
	}
	
	public void stepFrame() {
		StartTile.inst.popLemmings();
		moveLemmings();
	}

	
	public void registerLemmingObs(LemmingObserver lemObvserv) {
		for(LemmingObservable lems : lemmings) {
			lems.register(lemObvserv);
		}
	}
	
	
	// add blocks easily
	public void addTeleport(Coordinate tp1, Coordinate tp2) {
		board[tp1.getX()][tp1.getY()] = TeleportTile.inst;
		board[tp2.getX()][tp2.getY()] = TeleportTile.inst;
		TeleportTile.inst.add(tp1, tp2);
	}
	
	public void addBlock(int xStart, int yStart, int xEnd, int yEnd, FieldTileObservable tile) {
		for(int i = xStart; i < xEnd; i++) {
			for(int j = yStart; j < yEnd; j++) {
				board[i][j] = tile;
			}
		}
	}
	
	public void addLine(int length, int xStart, int y, FieldTileObservable tile) {
		if(xStart+length >= board.length)
			length = board.length-xStart;
		if(y == board.length)
			y = board.length-1;
		for(int i = 0; i < length; i++) {
			board[xStart+i][y] = tile;		
		}
	}
	
	public void addWall(int height, int x, int yStart, FieldTileObservable fieldTile) {
		for(int i = 0; i < height; i++) {
			if(i <= yStart)
				board[x][yStart-i] = fieldTile;
		}
	}
	
	public void destroyWall(int height, int x, int yStart) {
		if(x < 0)
			return;
		for(int i  = 1; i < height+1; i++) {
				board[x][yStart-i].effect(lemmings, x, yStart-i);
				board[x][yStart-i].destroy(this,height, x, yStart-i);
		}
	}
	
	public void destroyCircle(int posX, int posY, int rayon) {
		for(int i = 0; i < rayon+1; i++) {
			for(int j = -i; j < i+1;j++) {
				for(int k = -i; k < i+1; k++) {
					try {
						board[posX-j][posY-k].effect(lemmings, posX-j, posY-k);
						board[posX-j][posY-k].destroy(this, 1, posX-j, posY-k);
					}catch(ArrayIndexOutOfBoundsException e) {}
				}
			}
		}
	}
}
