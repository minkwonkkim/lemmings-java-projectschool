package lemmings;

public class App {

	private static final int FRAME_LOCATION_X = 100;
	private static final int FRAME_LOCATION_Y = 100;
	
	public static void main(String[] args) {

		Game game = new Game(FRAME_LOCATION_X, FRAME_LOCATION_Y, 500, 500);
		try {
			game.run();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
