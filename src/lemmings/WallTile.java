package lemmings;

import java.util.ArrayList;

public class WallTile extends FieldTileObservable{
	public static WallTile inst;
	
	public WallTile() {
		setUpFieldTileBehaviour(new FieldTile() {
			
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
			}

			@Override
			public boolean isTransparent() {
				return false;
			}
		});
	}

	public WallTile(FieldTile fieldTile) {
		super(fieldTile);
	}
}
