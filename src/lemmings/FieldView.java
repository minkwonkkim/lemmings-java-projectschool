package lemmings;


import java.awt.Color;

import java.awt.Graphics;

import java.util.ArrayList;


public class FieldView {

	public final static int WIDTH = 500;
	public final static int HEIGHT = 500;

	private final Field field;
	private ArrayList<LemmingObservable> lemmings;
	private final int dimBoard;

	public FieldView(Field field) {
		this.field = field;
		//to not set up a getter with pointer
		field.setUpLemmings(this);
		
		this.dimBoard = field.getDim();
		
		setUpFieldTilesDisplay();
	}
	
	public void setUpFieldTilesDisplay() {
		setUpAirTileDisplay();
		setUpSimpleWallTileDisplay();
		setUpStartTileDisplay();
		setUpConstructTileDisplay();
		setUpEndTileDisplay();
		setUpIndestructibleWallTileDisplay();
		setUpKillingTileDisplay();
		setUpTeleportTileDisplay();
		setUpLaveTileDisplay();
	}
	
	public void setUpLaveTileDisplay(){
		LavaTile.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(Color.RED);
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	
	public void setUpTeleportTileDisplay() {
		TeleportTile.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(Color.YELLOW);
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	
	public void setUpKillingTileDisplay() {
		KillingDWT.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(Color.ORANGE);
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	
	
	public void setUpIndestructibleWallTileDisplay() {
		IndestructibleWallTile.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(Color.BLACK);
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	
	public void setUpEndTileDisplay() {
		EndTile.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(new Color((float)0.9,(float)0.2,(float)0.5));
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	
	public void setUpConstructTileDisplay() {
		ConstructDWT.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(Color.PINK);
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	
	public void setUpStartTileDisplay() {
		StartTile.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(Color.GREEN);
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	public void setUpAirTileDisplay() {
		AirTile.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(Color.WHITE);
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	
	public void setUpSimpleWallTileDisplay() {
		SimpleDWT.inst.register(new FieldTileObserver() {
			@Override
			public void draw(Graphics g, int posX, int posY) {
				g.setColor(Color.DARK_GRAY);
				FieldTileDisplay.drawBaseTile(g, posX, posY, WIDTH, HEIGHT, dimBoard);
			}
		});
	}
	public void setLemmings(ArrayList<LemmingObservable> lemmings) {
		this.lemmings = lemmings;
	}
	
	void drawLemmings(Graphics g) {
		for(LemmingObservable l : lemmings) {
			//drawing Base of Lemming
			Coordinate lemPos = l.getPos();
			g.setColor(Color.LIGHT_GRAY);
			int x = WIDTH * lemPos.getX() / dimBoard;
			int xWidth =  WIDTH / dimBoard;
			int y = HEIGHT * lemPos.getY() / dimBoard;
			int yWidth = HEIGHT / dimBoard;
			g.fillRect(x, y, xWidth, yWidth);
			g.setColor(Color.BLACK);
			g.drawRect(x, y, xWidth, yWidth);
			
			//drawing Direction arrow
			int directX = (l.getDirectionX()+1)/2; //0 if left, 1 if right
			Coordinate BigLine = new Coordinate(x,y+(yWidth/2));
			Coordinate FirstDirectionLinePoint = new Coordinate(x+(xWidth/2),y);
			Coordinate SecondDirectionLinePoint = new Coordinate(x+(xWidth/2), y+yWidth);
			Coordinate ConvergeDirectionLinePoint = new Coordinate(x+(xWidth*directX), y+(yWidth/2));
			g.drawLine(BigLine.getX(), BigLine.getY(), BigLine.getX()+xWidth, BigLine.getY());
			g.drawLine(FirstDirectionLinePoint.getX(),FirstDirectionLinePoint.getY(),ConvergeDirectionLinePoint.getX(), ConvergeDirectionLinePoint.getY());
			g.drawLine(SecondDirectionLinePoint.getX(),SecondDirectionLinePoint.getY(),ConvergeDirectionLinePoint.getX(), ConvergeDirectionLinePoint.getY());
		
			//name of currentLemming
			int xString= (l.toString().length()*6-xWidth)/2;
			g.drawString(l.toString(), x-xString, y);
			
		}
	}

	void drawFields(Graphics g) {
		for(int i = 0; i < dimBoard; i++) {
			for(int j = 0; j < dimBoard; j++) {
				
				FieldTileObservable tile = field.getTypeFieldAt(i,j);
				tile.notifyDrawObservers(g,i,j);
			}
		}
	}
	
	public void makeMove(int tabx2, int taby2, LemmingState typeLem) {
		Coordinate tabCoord = new Coordinate(tabx2, taby2);
		field.makeMove(tabCoord, typeLem);
	}

}
