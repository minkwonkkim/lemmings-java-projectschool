package lemmings;

import java.util.ArrayList;
//pink
public class ConstructDWT extends DestructibleWallTile {
	public static ConstructDWT inst;
	ConstructToBuild[][] obs;
	
	public ConstructDWT(int dimBoard) {
		obs = new ConstructToBuild[dimBoard][dimBoard];
		
		setUpFieldTileBehaviour(new FieldTile() {
			@Override
			public void effect(ArrayList<LemmingObservable> lemmings, int x, int y) {
			}
			
			@Override
			public void destroy(Field field, int height, int x, int yStart) {
				destroy(field, height, x, yStart);
			}

			@Override
			public boolean isTransparent() {
				return false;
			}
		});
	}

	public ConstructDWT(FieldTile fieldTile) {
		super(fieldTile);
	}

	public void addNewConstructWall(Field f, char obstacleType, int obsLen, Coordinate constructWall, Coordinate toBuild, FieldTileObservable tileToSet) {
		f.addWall(1, constructWall.getX(), constructWall.getY(), this);
		obs[constructWall.getX()][constructWall.getY()] = new ConstructToBuild(tileToSet, obsLen, obstacleType, new Coordinate(toBuild.getX(), toBuild.getY()));
	}
	
	@Override
	public void destroy(Field field, int height, int x, int y) {
		ConstructToBuild curObs = obs[x][y];
		int obsLen = curObs.getObsLen();
		int startX = curObs.getObsPlacement().getX();
		int startY = curObs.getObsPlacement().getY();
		FieldTileObservable tileToSet = curObs.getObsType();
		
		switch(curObs.getObsShape()) {
			case '|' : field.addWall(obsLen, startX, startY, tileToSet); 
						break;
			case '_' : field.addLine(obsLen, startX,startY, tileToSet);
						break;
			case '[' : field.addBlock(startX, startY, startX+obsLen, startY+obsLen, tileToSet);
		}
		destroyParent(field, height, x, y);
	}
	
	
}
