package lemmings;

public class Grimpeur extends NormalLemming {

	boolean climbing;
	
	public Grimpeur(Lemming lem) {
		super(lem);
		climbing = false;
	}

	public Grimpeur() {
	}
	
	protected boolean marchingBehaviour(Field field) {
		Coordinate position = lem.getPos();
		Coordinate checkRoof = new Coordinate(position.getX(), position.getY()-1);
		Coordinate checkJumpingTile = new Coordinate(position.getX() + lem.getDirectionX(), position.getY());
		
		if(field.containTile(checkJumpingTile)) {
			checkJumpingTile.incY(-1);
			
			if(field.containTile(checkJumpingTile) && !(field.containTile(checkRoof))) {
				climbing = true;
				
				position.incY(-1);
				lem.setPos(position);
				return false;
			}
					
		}
		climbing = false;
		return super.marchingBehaviour(field);

	}
	
	@Override
	public void move(Field f) {
		if(climbing)
			marchingBehaviour(f);
		else
			super.move(f);
	}
	
	@Override
	public String toString() {
		return "Grimpeur";
	}
	
	public LemmingState copy(Lemming lem) {
		return new Grimpeur(lem);
	}
}
