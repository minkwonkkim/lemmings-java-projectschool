package lemmings;

public class NormalLemming extends LemmingState {

	public NormalLemming(Lemming lem) {
		super(lem);
	}
	
	public NormalLemming() {
	}
	
	protected boolean fallingBehaviour(Field field) {
		Coordinate position = lem.getPos();
		Coordinate checkFalling = new Coordinate(position.getX(), position.getY()+1);
	
		//checks if falls
		if(field.containTile(checkFalling)) {
			if(lem.getTotalCountFalling() > getMaxHeightFalling()) {
				lem.kill();
				return true;
			}else
				lem.setTotalCountFalling(0);
			return false;
		}
		
		if(field.isOut(checkFalling.getY()))
		  	lem.kill();
		else{
		  	lem.setPos(checkFalling);
			field.tileEffectAt(checkFalling.getX(), checkFalling.getY());
			lem.setTotalCountFalling(lem.getTotalCountFalling()+1);
		}
		return true;
		
	}
	
	protected boolean marchingBehaviour(Field field) {
		Coordinate position = lem.getPos();
		Coordinate nextTile = new Coordinate(position.getX() + lem.getDirectionX(), position.getY());
		Coordinate checkRoofTile;
		
		if(field.containBlocker(nextTile)) {
			lem.changeDirection();
			return false;
		}
		
		for(int i = 0; i < getJumpingHeight(); i++) {
			if(field.containTile(nextTile))
				nextTile.incY(-1);
			else{
				checkRoofTile = new Coordinate(nextTile.getX()-lem.getDirectionX(), nextTile.getY());
				if(!field.containTile(checkRoofTile)) {
					lem.setPos(nextTile);
					field.tileEffectAt(nextTile.getX(),  nextTile.getY());
					return true;
				}
			}
				
		}
		lem.changeDirection();
		return false;
	}

	@Override
	public String toString() {
		return "Normal";
	}
	
	@Override
	public LemmingState copy(Lemming lem) {
		return new NormalLemming(lem);
	}

	
	
}
