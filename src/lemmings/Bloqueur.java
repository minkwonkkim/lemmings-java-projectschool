package lemmings;

public class Bloqueur extends NormalLemming{

	public Bloqueur() {
		
	}

	public Bloqueur(Lemming lem) {
		super(lem);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean marchingBehaviour(Field field) {
		return false;
	}
	
	@Override
	public String toString() {
		return "Bloqueur";
	}
	
	public LemmingState copy(Lemming lem) {
		return new Bloqueur(lem);
	}
	
	@Override
	public boolean blocks() {
		return true;
	}
}
