package lemmings;




import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Game {
	private JFrame frame = new JFrame("Lemmings");
	private Field field;
	private FieldView fieldV;
	private Interface intf;
	private InterfaceView intfv;
	private GameScreen gs;
	private static final int GAMESPEED = 1000;
	
	public Game(int frame_x, int frame_y, int field_w, int field_h) {
		
		setUpField();

		intf 	= new Interface();
		intfv 	= new InterfaceView(intf);
		
		gs = new GameScreen(fieldV, intfv, field.getDim());
		
		setUpFrame(gs, frame_x, frame_y);
	}
	public void setUpField(){
		try {
			field 	= new Field();
		}catch(Exception e) {
			JOptionPane.showMessageDialog(new JFrame("Pas d'entrée"), "Ajoutez au moins une entrée dans setUpStartingPoints()");
			System.exit(0);
		}
		fieldV 	= new FieldView(field);
		
		field.registerLemmingObs(new LemmingObserver() {			
			@Override
			public void update() {
				gs.repaint();
			}
		});
		
	}
	
	private void setUpFrame(GameScreen gs, int frame_x, int frame_y) {
		frame.add(gs);
		frame.setSize(gs.getSize());
		frame.setLocation(frame_x, frame_y);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public void run() throws InterruptedException {
		while(field.curLemmingDead()+field.curLemmingsEnd() < field.nbLemmings()) {
			Thread.sleep(GAMESPEED);
			field.stepFrame();
			gs.repaint();
		}
		if(field.curLemmingsEnd() == 0) {
			JOptionPane.showMessageDialog(new JFrame("Partie finie !"), "Perdu ! Vous avez perdu tous vos Lemmings");
		}else {
			JOptionPane.showMessageDialog(new JFrame("Partie finie !"), "Fini ! Vous avez sauvé "+field.curLemmingsEnd()+" Lemmings et "+field.curLemmingDead()+" sont morts");
		}
	}
}
